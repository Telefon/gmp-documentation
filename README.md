# How to use

## Visual Studio Code
Download this repository, install the Lua Language Server extension from sumneko, go to the extension settings, scroll down to `Lua > Workspace: Library` at the bottom of the page and enter the path to this folder.

### Recommended procedure if you're using Git
Navigate to your repository, open a console and add this repository as a submodule:
```bash
git submodule add https://gitlab.com/Telefon/GMP-Documentation.git doc
```

Then create the file `.luarc.json` in the root path of your repository and add the following lines:
```json
{
	"workspace.library": [
		"doc/"
	]
}
```

If there are updates to this repository, you can update your definitions just with
```bash
git submodule update --remote doc
```

## Important: Don't include any of these files in your scripts!
