--- @meta
--- Gothic Multiplayer (GMP) API function documentation.
--- Wiki: @see https://gitlab.com/Reveares/GMP/-/wikis/Functions

--------------------------------------------------------------------------------
------------------------------------- NPC --------------------------------------
--------------------------------------------------------------------------------

--- Creates an NPC with that name and returns its ID. Default instance is
--- "PC_HERO".
--- @param name string
--- @return integer npcid
function CreateNPC(name) end

--- Destroys the NPC with the given ID.
--- @param npcid integer
--- @return integer npcid
function DestroyNPC(npcid) end

--- Returns if the player is an NPC.
--- @param playerid integer
--- @return 0|1
function IsNPC(playerid) end


--------------------------------------------------------------------------------
------------------------------------ Timer -------------------------------------
--------------------------------------------------------------------------------

--- @param func function|string The callback function.
--- @param milliseconds integer Delay between calls.
--- @param _repeat 0|1 Repeat timer or execute just once.
--- @param ... any Arguments to the callback function.
--- @return integer timerid
function SetTimer(func, milliseconds, _repeat, ...) end

--- @param func function|string The callback function.
--- @param milliseconds integer Delay between calls.
--- @param _repeat 0|1 Repeat timer or execute just once.
--- @param ... any Arguments to the callback function.
--- @return integer timerid
function SetTimerEx(func, milliseconds, _repeat, ...) end

--- Kills a timer.
--- @param timerid integer
function KillTimer(timerid) end

--- Returns if a timer is active.
--- @param timerid integer
--- @return 0|1 active
function IsTimerActive(timerid) end

--------------------------------------------------------------------------------
------------------------------------- Draw -------------------------------------
--------------------------------------------------------------------------------

--- Creates a server wide draw that can be displayed for players via
--- `ShowDraw()`. `x` and `y` refer to the virtual pixels in the range 0-8192
--- from the upper left corner. If centered is set to 0 (the default), the
--- text starts at the given position, otherwise it's centered to this position.
--- Returns a number that identifies this draw.
--- @param x integer
--- @param y integer
--- @param text string
--- @param font? string Default = "Font_Old_10_White_Hi.tga"
--- @param r? integer Default = 255
--- @param g? integer Default = 255
--- @param b? integer Default = 255
--- @param a? integer Default = 255
--- @param centered? 0|1 Default = 0
--- @return integer drawid
function CreateDraw(x, y, text, font, r, g, b, a, centered) end

--- @param drawid integer
--- @param x integer
--- @param y integer
--- @param text string
--- @param font string
--- @param r integer
--- @param g integer
--- @param b integer
--- @param a integer Default = 255
function UpdateDraw(drawid, x, y, text, font, r, g, b, a) end

--- Sets the text of a draw.
--- @param drawid integer
--- @param text string
function SetDrawText(drawid, text) end

--- Returns the text of the draw.
--- @param drawid integer
--- @return string text
function GetDrawText(drawid) end

--- Sets the position of a draw in virtual pixels (0-8192) from the upper left
--- corner.
--- @param drawid integer
--- @param x integer
--- @param y integer
function SetDrawPos(drawid, x, y) end

--- Returns the position of the draw in virtual pixels (0-8192) from the upper
--- left corner.
--- @param drawid integer
--- @return integer x, integer y
function GetDrawPos(drawid) end

--- Sets the color of a draw in RGBA.
--- @param drawid integer
--- @param r integer
--- @param g integer
--- @param b integer
--- @param a integer Default = 255
function SetDrawColor(drawid, r, g, b, a) end

--- Returns the color of the draw in RGBA.
--- @param drawid integer
--- @return integer r, integer g, integer b, integer a
function GetDrawColor(drawid) end

--- Shows a draw for a player.
--- @param playerid integer
--- @param drawid integer
function ShowDraw(playerid, drawid) end

--- Hides a draw for a player.
--- @param playerid integer
--- @param drawid integer
function HideDraw(playerid, drawid) end

--- Returns if the draw is visible for the given player.
--- @param playerid integer
--- @param drawid integer
--- @return 0|1 visible
function IsDrawVisible(playerid, drawid) end

--- Destroys the draw with the given ID.
--- @param drawid integer
function DestroyDraw(drawid) end

--- Prints a text for the given period in milliseconds on the screen of all
--- currently connected players. x and y refer to the virtual pixels in the
--- range 0-8192 from the upper left corner. If centered is set to 0 (the
--- default), the text starts at the given position, otherwise it's centered
--- to this position.
---
--- Game texts are equivalent to a normal draw that you destroy after some
--- time, but with less overhead. However, note that only one game text can be
--- displayed at the same time.
--- @param x integer
--- @param y integer
--- @param text string
--- @param font string
--- @param r integer
--- @param g integer
--- @param b integer
--- @param time integer
--- @param centered? 0|1 default = 0
function GameTextForAll(x, y, text, font, r, g, b, time, centered) end


--------------------------------------------------------------------------------
---------------------------------- PlayerDraw ----------------------------------
--------------------------------------------------------------------------------

--- Creates a draw only for a certain player. This saves memory for the server
--- and the players that don't need that draw. x and y refer to the virtual
--- pixels in the range 0-8192 from the upper left corner. If centered is set
--- to 0 (the default), the text starts at the given position, otherwise it's
--- centered to this position. Returns a number that identifies this
--- player-draw.
--- @param playerid integer
--- @param x integer
--- @param y integer
--- @param text string
--- @param font? string Default = "Font_Old_10_White_Hi.tga"
--- @param r? integer Default = 255
--- @param g? integer Default = 255
--- @param b? integer Default = 255
--- @param a? integer Default = 255
--- @param centered? 0|1 Default = 0
--- @return integer drawid
function CreatePlayerDraw(playerid, x, y, text, font, r, g, b, a, centered) end

--- @param playerid integer
--- @param drawid integer
--- @param x integer
--- @param y integer
--- @param text string
--- @param font string
--- @param r integer
--- @param g integer
--- @param b integer
--- @param a? integer Default = 255
function UpdatePlayerDraw(playerid, drawid, x, y, text, font, r, g, b, a) end

--- Sets the text of a playerdraw.
--- @param playerid integer
--- @param playerdrawID integer
--- @param text string
function SetPlayerDrawText(playerid, playerdrawID, text) end

--- Sets the position of a playerdraw in virtual pixels (0-8192) from the
--- upper left corner.
--- @param playerid integer
--- @param playerdrawID integer
--- @param x integer
--- @param y integer
function SetPlayerDrawPos(playerid, playerdrawID, x, y) end

--- Sets the color of a playerdraw in RGBA.
--- @param playerid integer
--- @param playerdrawID integer
--- @param r integer
--- @param g integer
--- @param b integer
--- @param a? integer Default = 255
function SetPlayerDrawColor(playerid, playerdrawID, r, g, b, a) end

--- Shows a playerdraw for the player.
--- @param playerid integer
--- @param playerdrawID integer
function ShowPlayerDraw(playerid, playerdrawID) end

--- Hides a playerdraw for the player.
--- @param playerid integer
--- @param playerdrawID integer
function HidePlayerDraw(playerid, playerdrawID) end

--- Destroys the playerdraw with the given ID.
--- @param playerid integer
--- @param playerdrawID integer
function DestroyPlayerDraw(playerid, playerdrawID) end

--- Destroys all players draws for a player. That does not include normal draws
--- that can be only deleted via `DestroyDraw()`.
--- @param playerid integer
function DestroyAllPlayerDraws(playerid) end

--- Prints a text for the given period in milliseconds on the player's screen.
--- `x` and `y` refer to the virtual pixels in the range 0-8192 from the upper
--- left corner. If centered is set to 0 (the default), the text starts at the
--- given position, otherwise it's centered to this position.
---
--- Game texts are equivalent to a normal draw that you destroy after some time,
--- but with less overhead. However, note that only one game text can be
--- displayed at the same time.
--- @param playerid integer
--- @param x integer
--- @param y integer
--- @param text string
--- @param font string
--- @param r integer
--- @param g integer
--- @param b integer
--- @param time integer
--- @param centered? 0|1 default = 0
function GameTextForPlayer(playerid, x, y,
	text, font, r, g, b, time, centered) end


--------------------------------------------------------------------------------
----------------------------------- Texture ------------------------------------
--------------------------------------------------------------------------------

--- Creates a texture at the given virtual pixels in the range 0-8192. Note that
--- toX and toY are not the width and the height of that draw, but the position
--- to which the texture should be displayed.
--- @param fromX integer
--- @param fromY integer
--- @param toX integer
--- @param toY integer
--- @param fileName string
--- @return integer textureid
function CreateTexture(fromX, fromY, toX, toY, fileName) end

--- Updates the texture with the given ID.
--- @param fromX integer
--- @param fromY integer
--- @param toX integer
--- @param toY integer
--- @param fileName string
function UpdateTexture(textureid, fromX, fromY, toX, toY, fileName) end

--- Shows a texture for the player.
--- @param playerid integer
--- @param textureid integer
function ShowTexture(playerid, textureid) end

--- Hides a texture for the player.
--- @param playerid integer
--- @param textureid integer
function HideTexture(playerid, textureid) end

--- Destroys the texture with the given ID.
--- @param textureid integer
function DestroyTexture(textureid) end


--------------------------------------------------------------------------------
------------------------------------ Cursor ------------------------------------
--------------------------------------------------------------------------------

--- Shows or hides the ingame cursor for the specified player. By default, the
--- cursor is invisible.
--- @param playerid integer
--- @param visible 0|1
function SetCursorVisible(playerid, visible) end

--- Sets the position of the player's cursor in virtual pixels in the range
--- 0-8192 from the upper left corner. The initial position is {4096/4096}.
--- @param playerid integer
--- @param x integer
--- @param y integer
function SetCursorPos(playerid, x, y) end

--- Sets the sensitivity of the player's cursor. The default is 3.5.
--- @param playerid integer
--- @param sensitivity number
function SetCursorSensitivity(playerid, sensitivity) end

--- Sets the texture of the player's cursor. The default is "LO.TGA".
--- @param playerid integer
--- @param filename string
function SetCursorTexture(playerid, filename) end


--------------------------------------------------------------------------------
------------------------------------ Sound -------------------------------------
--------------------------------------------------------------------------------

--- Creates a sound that can be played for players.
--- @param filename string
--- @return integer soundid
function CreateSound(filename) end

--- @param playerid integer
--- @param soundid integer
function PlayPlayerSound(playerid, soundid) end


--- @param playerid integer
--- @param soundid integer
--- @param x number
--- @param y number
--- @param z number
--- @param radius number
function PlayPlayerSound3D(playerid, soundid, x, y, z, radius) end

--- @param playerid integer
--- @param soundid integer
function StopPlayerSound(playerid, soundid) end

--- Destroys the sound with the given ID.
--- @param soundid integer
function DestroySound(soundid) end


--------------------------------------------------------------------------------
------------------------------------- Item -------------------------------------
--------------------------------------------------------------------------------

--- Creates an item of the given instance and amount in a world at the position
--- {x, y, z}. Optional parameters are the rotation and if the item should have
--- physics enabled.
--- @param instance string
--- @param amount integer
--- @param x number
--- @param y number
--- @param z number
--- @param world string
--- @param rotX? number default = 0
--- @param royY? number default = 0
--- @param rotZ? number default = 0
--- @param gravitation? 0|1 default = 1
--- @return integer itemid
function CreateItem(instance, amount, x, y, z, world,
	rotX, royY, rotZ, gravitation) end

--- Destroys the item with the given ID.
--- @param itemid integer
function DestroyItem(itemid) end

--- @param playerid integer
--- @param instance string
--- @param checkid integer
function HasItem(playerid, instance, checkid) end

--- Gives a player the item with the given instance and amount.
--- @param playerid integer
--- @param instance string
--- @param amount integer
function GiveItem(playerid, instance, amount) end

--- @param playerid integer
--- @param instance string
--- @param amount integer
function RemoveItem(playerid, instance, amount) end

--- @param playerid integer
--- @param slot integer
--- @param amount integer
function RemoveItemBySlot(playerid, slot, amount) end

--- Lets a player drop an item.
--- @param playerid integer
--- @param instance string
--- @param amount integer
function DropItem(playerid, instance, amount) end

--- Drops all items of the given inventory slot.
--- @param playerid integer
--- @param slot integer
function DropItemBySlot(playerid, slot) end

--- @param playerid integer
--- @param slot integer
function GetPlayerItem(playerid, slot) end

--- @param itemid integer
--- @return table<string>
function GetWorldItem(itemid) end

--- Returns whether an item with the given ID exists in any world.
--- @param itemid integer
--- @return 0|1 exists
function DoesWorldItemExist(itemid) end


--------------------------------------------------------------------------------
------------------------------------- Vob --------------------------------------
--------------------------------------------------------------------------------

--- @class Vob
Vob = {}

--- @param visual string
--- @param world string
--- @param x number
--- @param y number
--- @param z number
--- @return Vob
function Vob.Create(visual, world, x, y, z) end

--- @return integer num
function Vob.GetNumberOfVobs() end

--- @param vob Vob
--- @return integer guid
function Vob.GetGUID(vob) end

--- @param vob Vob
--- @param visual string
function Vob.SetVisual(vob, visual) end

--- @param vob Vob
--- @param x number
--- @param y number
--- @param z number
function Vob.SetPosition(vob, x, y, z) end

--- @param vob Vob
--- @return number x, number y, number z
function Vob.GetPosition(vob) end

--- @param vob Vob
--- @param x number
--- @param y number
--- @param z number
function Vob.SetRotation(vob, x, y, z) end

--- @param vob Vob
--- @return number x, number y, number z
function Vob.GetRotation(vob) end

--- @param vob Vob
--- @param visible 0|1
function Vob.SetVisible(vob, visible) end

--- @param vob Vob
--- @return 0|1 visible
function Vob.IsVisible(vob) end

--- @param vob Vob
--- @param collision integer
function Vob.SetCollision(vob, collision) end

--- @param vob Vob
--- @return integer collision
function Vob.GetCollision(vob) end

--- @param vob Vob
function Vob.Destroy(vob) end

function Vob.DestroyAllVobs() end


--------------------------------------------------------------------------------
------------------------------------- Mob --------------------------------------
--------------------------------------------------------------------------------

--- @class Mob
Mob = {}

--- If you take the name from the Client Scripts found in
--- `Scripts\content\Story\Text.d` the name will actually be displayed on top
--- of the spawned mobsi.
--- @param visual string
--- @param name string
--- @param type integer
--- @param scheme string
--- @param useWithItem string
--- @param world string
--- @param x number
--- @param y number
--- @param z number
--- @param objectName? string default = ""
--- @return Mob mob
function Mob.Create(visual, name, type, scheme,
	useWithItem, world, x, y, z, objectName) end

--- @return integer num
function Mob.GetNumberOfMobs() end

--- @param mob Mob
--- @return integer guid
function Mob.GetGUID(mob) end

--- @param mob Mob
--- @param visual string
function Mob.SetVisual(mob, visual) end

--- @param mob Mob
--- @param name string
function Mob.SetName(mob, name) end

--- Sets the object name of a mob, which can be used to identify a mob in
--- `OnPlayerTriggerMob`.
--- @param mob Mob
--- @param objectName string
function Mob.SetObjectName(mob, objectName) end

--- Returns the object name of a mob which can be used to identify the mob in
--- `OnPlayerTriggerMob`.
--- @param mob Mob
--- @return string name
function Mob.GetObjectName(mob) end

--- @param mob Mob
--- @param type integer
--- @param scheme string
function Mob.SetScheme(mob, type, scheme) end

--- @param mob Mob
--- @param instance string
function Mob.SetUseWithItem(mob, instance) end

--- @param mob Mob
--- @param visible 0|1
function Mob.SetVisible(mob, visible) end

--- @param mob Mob
--- @return 0|1 visible
function Mob.IsVisible(mob) end

--- @param mob Mob
--- @param collision integer 0|1
function Mob.SetCollision(mob, collision) end

--- @param mob Mob
--- @return 0|1 collision
function Mob.GetCollision(mob) end

--- @param mob Mob
--- @param x number
--- @param y number
--- @param z number
function Mob.SetPosition(mob, x, y, z) end

--- @param mob Mob
--- @return number x, number y, number z
function Mob.GetPosition(mob) end

--- @param mob Mob
--- @param x number
--- @param y number
--- @param z number
function Mob.SetRotation(mob, x, y, z) end

--- @param mob Mob
--- @return number x, number y, number z
function Mob.GetRotation(mob) end

--- @param mob Mob
function Mob.Destroy(mob) end

function Mob.DestroyAllMobs() end

--------------------------------------------------------------------------------
---------------------------------- Inventory -----------------------------------
--------------------------------------------------------------------------------

--- Opens the inventory of a player.
--- @param playerid integer
function OpenInventory(playerid) end

--- Closes the inventory of a player.
--- @param playerid integer
function CloseInventory(playerid) end

--- Deletes all items that a player has.
--- @param playerid integer
function ClearInventory(playerid) end


--------------------------------------------------------------------------------
------------------------------------ Camera ------------------------------------
--------------------------------------------------------------------------------

--- @param playerid1 integer
--- @param playerid2 integer
--- @return integer
function SetCameraBehindPlayer(playerid1, playerid2) end

--- @param playerid integer
--- @param vob Vob
function SetCameraBehindVob(playerid, vob) end

--- @param playerid integer
function SetDefaultCamera(playerid) end

--- If set to 1, the camera won't follow the player if he moves.
--- @param playerid integer
--- @param toggle 0|1
function FreezeCamera(playerid, toggle) end


--------------------------------------------------------------------------------
----------------------------------- Walkmode -----------------------------------
--------------------------------------------------------------------------------

--- Sets the walk mode of a player, e.g. "HumanS_Relaxed.mds".
--- @param playerid integer
--- @param walk string
function SetPlayerWalk(playerid, walk) end

--- Returns the current walk mode of a player, e.g. "HumanS_Relaxed.mds".
--- @param playerid integer
--- @return string walk
function GetPlayerWalk(playerid) end

--- @param playerid integer
--- @param overlay string
function RemovePlayerOverlay(playerid, overlay) end


--------------------------------------------------------------------------------
---------------------------------- Animation -----------------------------------
--------------------------------------------------------------------------------

--- @param playerid integer
--- @param aniname string
function PlayAnimation(playerid, aniname) end

--- Returns the name of the animation that a player performs.
--- @param playerid integer
--- @return string aniname
function GetPlayerAnimationName(playerid) end

--- Returns the ID of the animation that a player performs. Note that the ID
--- depends on the HumanS.mds of the player and will be different if you insert
--- or delete new animations.
--- @param playerid integer
--- @return integer animid
function GetPlayerAnimationID(playerid) end

--- @param playerid integer
function PlayGesticulation(playerid) end


--------------------------------------------------------------------------------
------------------------------------ World -------------------------------------
--------------------------------------------------------------------------------

--- Sets the ingame time for all players.
--- @param hour integer
--- @param minute integer
function SetTime(hour, minute) end

--- Returns the Gothic time.
--- @return integer hour, integer minute
function GetTime() end

--- Opens or closes all locks on the server, such as chests and doors.
--- @param toggle 0|1
function OpenLocks(toggle) end

--- Turns the magic barrier from Gothic I for `durationSecs` seconds on. To see
--- it, you must be spawned.
--- @param durationSecs number
--- @param fadeSecs number
--- @param thunderDistSecs number
--- @param thunderJitterSecs number
function SetBarrier(durationSecs, fadeSecs,
	thunderDistSecs, thunderJitterSecs) end

--- Controls the ingame weather for the given period. If you don't call this
--- function again, the weather will stay like this every day. Note that sunny
--- is not a weather, but is defined as the absence of one. So if it rains from
--- 13:00 to 17:00, the sun (or the moon) shines the rest of the day.
--- `lightning` is a bool that specifies if there should be thunder, only works
--- with rain. The time can range from the current day at 12:00 until the next
--- day at 11:59, so you can let it rain/snow from e.g. 19:00 to 05:00 at the
--- next day, but you can't let it rain/snow from 06:00 to 14:00. This is a
--- restriction of the Gothic engine.
--- @param weather weathertype
--- @param lightning 0|1
--- @param startHour hourtype
--- @param startMinute minutetype
--- @param endHour hourtype
--- @param endMinute minutetype
function SetWeather(weather, lightning,
	startHour, startMinute, endHour, endMinute) end

--- Sets the virtual world of a player to any number. A virtual world is
--- client-side the same world with the same server vobs and mobs, but a player
--- can only see players in the same virtual world.
--- @param playerid integer
--- @param world integer
function SetPlayerVirtualWorld(playerid, world) end

--- @param playerid integer
--- @return integer virtualworld
function GetPlayerVirtualWorld(playerid) end

--- @param world integer
function SetVirtualWorldForAllConnected(world) end


--------------------------------------------------------------------------------
------------------------------------ Server ------------------------------------
--------------------------------------------------------------------------------

--- @param name string
function SetGamemodeName(name) end

--- @param text string
function SetServerDescription(text) end

--- @param text string
function SetServerHostname(text) end

--- @return string[] addresses
function GetListeningAddresses() end

--- Kick a player from the server.
--- @param playerid integer
function Kick(playerid) end

--- Bans a player from the server and saves his name, Serial, IP address and
--- MAC address in banlist.ban.
--- @param playerid integer
function Ban(playerid) end

--- Returns the ping of a player. -2 means that he isn't connected and -1 means
--- that he is an NPC.
--- @param playerid integer
--- @return integer ping
function GetPlayerPing(playerid) end

--- Sets if a player should be streamable/visible for other players.
--- @param playerid integer
--- @param streamable 0|1
function SetPlayerStreamable(playerid, streamable) end

--- Returns if a player can be streamed by other players.
--- @param playerid integer
--- @return 0|1 streamable
function IsPlayerStreamable(playerid) end

--- @param playerid integer
--- @param r integer
--- @param g integer
--- @param b integer
--- @param text string
function SendPlayerMessage(playerid, r, g, b, text) end

--- @param r integer
--- @param g integer
--- @param b integer
--- @param text string
function SendMessageToAll(r, g, b, text) end

--- Enables or disables the event `OnPlayerUpdate()`.
--- @param toggle integer
function Enable_OnPlayerUpdate(toggle) end

--- @param playerid integer
--- @param toggle 0|1
function SetPlayerEnableOnUpdate(playerid, toggle) end

--- @param playerid integer
--- @return 0|1 enabled
function GetPlayerEnableOnUpdate(playerid) end


--------------------------------------------------------------------------------
------------------------------------ Limits ------------------------------------
--------------------------------------------------------------------------------

--- Returns the maximum amount of possible players, defined in `gmp_server.ini`.
--- @return integer num
function GetMaxPlayers() end

--- Returns the maximum amount of possible bots, defined in `gmp_server.ini`.
--- @return integer num
function GetMaxBots() end

--- Returns the maximum amount of possible IDs,
--- `GetMaxBots() + GetMaxPlayers()`.
--- @return integer num
function GetMaxSlots() end

--- Returns the maximum amount of possible timers at the same time, defined in
--- `gmp_server.ini`.
--- @return integer num
function GetMaxTimers() end

--- Returns the maximum amount of possible draws at the same time, defined in
--- `gmp_server.ini`.
--- @return integer num
function GetMaxDraws() end

--- Returns the maximum amount of possible textures at the same time, defined in
--- `gmp_server.ini`.
--- @return integer num
function GetMaxTextures() end

--- Returns the maximum amount of possible sounds at the same time, defined in
--- `gmp_server.ini`.
--- @return integer num
function GetMaxSounds() end

--- Returns the maximum amount of items, that can exist in the world, defined in
--- `gmp_server.ini`.
--- @return integer num
function GetMaxWorldItems() end


--------------------------------------------------------------------------------
------------------------------------- Menu -------------------------------------
--------------------------------------------------------------------------------

--- Enables or distables the builtin GMP chat system. If you disable it, you can
--- write your own.
--- @param toggle 0|1
function EnableChat(toggle) end

--- Shows or hides the ingame GMP chat. Note that the player can reopen it via
--- F12.
--- @param playerid integer
--- @param toggle 0|1
function ShowChat(playerid, toggle) end

--- @param playerid integer
function PlayerOpenChat(playerid) end

--- Opens an input field (just like the chat input) for the player at the given
--- x and y coordinates and calls OnPlayerInputField when the player enters
--- some text.
---
--- `maxWidth`: optional (can be `nil`), after what text width a line break
--- should occur (at a space)
---
--- `centered`: optional, if the text should be centered to the given position
---
--- `replacementSymbol`: optional, the symbol that should be displayed instead
--- of the actual entered character (useful for passwords)
--- @param playerid integer
--- @param x integer
--- @param y integer
--- @param maxWidth? integer
--- @param centered? 0|1 Default = 0
--- @param replacementSymbol? string
function OpenInputField(playerid, x, y,
	maxWidth, centered, replacementSymbol) end

--- If you disable it, the specific player can't open the playerlist via F5.
--- @param playerid integer
--- @param toggle 0|1
function EnablePlayerPlayerlist(playerid, toggle) end

--- If you disable it, a player can't open the debug menu via F6.
--- @param playerid integer
--- @param toggle 0|1
function EnablePlayerDebugMenu(playerid, toggle) end

--- Enables or disables the ingame menu globally. To set it for a specific
--- player, see `SetPlayerEnableGMPMenu()`.
--- @param enable 0|1
function EnableGMPMenu(enable) end

--- Enables or disables the ingame menu for a specific player. To set it
--- globally, see `EnableGMPMenu()`.
--- @param playerid integer
--- @param enable 0|1
function SetPlayerEnableGMPMenu(playerid, enable) end

--- @param playerid integer
function OpenGMPMenu(playerid) end

--- Enables or disables the ability for a player to exit the game via the GMP
--- menu or the commands `/q`, `/quit` and `/exit`. If you disable it, you can
--- write the commands yourself, e.g. with a delay.
--- @param toggle 0|1
function EnableExitGame(toggle) end

--- @param playerid integer
--- @param toggle 0|1
function SetPlayerEnableExitGame(playerid, toggle) end

--- Exits the player's game.
--- @param playerid integer
function ExitGame(playerid) end

--- Enables or disables the event `OnPlayerKey`.
--- @param toggle 0|1
function Enable_OnPlayerKey(toggle) end

--- This functions enables or disables the callback OnPlayerKey for a specific
--- player.
--- @param playerid integer
--- @param toggle 0|1
function SetPlayerEnable_OnPlayerKey(playerid, toggle) end

--- Returns if key interactions of a player will call `OnPlayerKey`.
--- @param playerid integer
--- @return integer
function GetPlayerEnable_OnPlayerKey(playerid) end

--- If you disable the nickname ID, the specific player can't see the ID of the
--- player in his focus, only the nickname.
--- @param playerid integer
--- @param toggle 0|1
function EnablePlayerNicknameID(playerid, toggle) end

--- If you disable the nickname ID, no player can see the ID of the player in
--- his focus, only the nickname.
--- @param toggle 0|1
function EnableNicknameID(toggle) end

--- If you disable it, the specific player can't see the nickname nor the ID of
--- the player in his focus.
--- @param playerid integer
--- @param toggle 0|1
function EnablePlayerNickname(playerid, toggle) end

--- If you disable the nickname, no player can't see the nickname nor the ID of
--- the player in his focus.
--- @param toggle 0|1
function EnableNickname(toggle) end

--- @param font string
function SetNicknameFont(font) end

--- @param playerid integer
--- @return 0|1 enabled
function IsEnabledPlayerNicknameID(playerid) end

--- @param playerid integer
--- @return 0|1 enabled
function IsEnabledPlayerNickname(playerid) end

--------------------------------------------------------------------------------
------------------------------------ Utils -------------------------------------
--------------------------------------------------------------------------------

--- Returns the number of milliseconds that have passed since the server's
--- computer started.
--- @return integer milliseconds
function GetTickCount() end

--- @param text string
--- @param format string
--- @return 0|1 succeeded, any ...
function sscanf(text, format) end

--- @param filename string
--- @param text string
function LogString(filename, text) end

--- Returns only the horizontal distance between the given coordinates in
--- centimeters.
--- @param x1 number
--- @param z1 number
--- @param x2 number
--- @param z2 number
--- @return number centimeters
function GetDistance2D(x1, z1, x2, z2) end

--- Returns the distance between the given coordinates in centimeters.
--- @param x1 number
--- @param y1 number
--- @param z1 number
--- @param x2 number
--- @param y2 number
--- @param z2 number
--- @return number centimeters
function GetDistance3D(x1, y1, z1, x2, y2, z2) end

--- Returns the distance between 2 players in centimeters.
--- @param playerid1 integer
--- @param playerid2 integer
--- @return number centimeters
function GetDistancePlayers(playerid1, playerid2) end

--- Calculates the angle between the given positions.
--- @param x1 number
--- @param z1 number
--- @param x2 number
--- @param z2 number
--- @return integer angle
function GetAngleToPos(x1, z1, x2, z2) end

--- Calculates the angle that `playerid1` needs to focus `playerid2`.
--- @param playerid1 integer
--- @param playerid2 integer
--- @return integer angle
function GetAngleToPlayer(playerid1, playerid2) end

--- Calculates the angle between the player and the given position.
--- @param playerid integer
--- @param x number
--- @param y number
--- @return integer angle
function GetPlayerAngleTo(playerid, x, y) end

--- Sets the HP and Mana to the players maximum.
--- @param playerid integer
function CompleteHeal(playerid) end

--- Splits a text beginning with "/" (often a command in `OnPlayerCommandText`)
--- in the pure command and the params.
--- @param text string
--- @return string command, string params
function GetCommand(text) end

--- Calcutates the MD5 hash of the player's file and then calls
--- `OnPlayerMD5File`.
--- @param playerid integer
--- @param filename string
function GetMD5File(playerid, filename) end

--- Calculates the MD5 hash of the given string.
--- @param text string
--- @return string hash
function MD5(text) end

--- Calculates the SHA1 hash of the given string.
--- @param text string
--- @return string hash
function SHA1(text) end

--- Calculates the SHA256 hash of the given string.
--- @param text string
--- @return string hash
function SHA256(text) end

--- Calculates the SHA384 hash of the given string.
--- @param text string
--- @return string hash
function SHA384(text) end

--- Calculates the SHA512 hash of the given string.
--- @param text string
--- @return string hash
function SHA512(text) end


--------------------------------------------------------------------------------
------------------------------------ Player ------------------------------------
--------------------------------------------------------------------------------

--- Sets the color of the player's name that is shown in the playerlist and the
--- focus in the RGB format.
--- @param playerid integer
---@param r integer
---@param g integer
---@param b integer
function SetPlayerColor(playerid, r, g, b) end

--- Returns the color of a player's name in the RGB format.
--- @param playerid integer
--- @return integer r, integer g, integer b
function GetPlayerColor(playerid) end

--- Sets the maximum health points of a player. This function calls
--- `OnPlayerChangeMaxHealth`.
--- @param playerid integer
--- @param value integer
function SetPlayerMaxHealth(playerid, value) end

--- Returns the maximum health points of a player.
--- @param playerid integer
--- @return integer maxhealth
function GetPlayerMaxHealth(playerid) end

--- Sets the health of a player and calls `OnPlayerChangeHealth()`.
--- If `value` <= 0, the player dies and `OnPlayerDeath` is called.
--- @param playerid integer
--- @param value integer
function SetPlayerHealth(playerid, value) end

--- Returns the health points of a player.
--- @param playerid integer
--- @return integer health
function GetPlayerHealth(playerid) end

--- Sets the maximum Mana of a player. This function calls
--- `OnPlayerChangeMaxMana`.
--- @param playerid integer
--- @param value integer
function SetPlayerMaxMana(playerid, value) end

--- Returns the maximum mana of a player.
--- @param playerid integer
--- @return integer maxmana
function GetPlayerMaxMana(playerid) end

--- Sets the Mana of a player. This function calls `OnPlayerChangeMana`.
--- @param playerid integer
--- @param value integer
function SetPlayerMana(playerid, value) end

--- Returns the mana of a player.
--- @param playerid integer
--- @return integer mana
function GetPlayerMana(playerid) end

--- Sets the dexterity of a player and calls `OnPlayerChangeDexterity`.
--- @param playerid integer
--- @param value integer
function SetPlayerDexterity(playerid, value) end

--- Returns the dexterity of a player.
--- @param playerid integer
--- @return integer dexterity
function GetPlayerDexterity(playerid) end

--- Sets the strength of a player. This function calls `OnPlayerChangeStrength`.
--- @param playerid integer
--- @param value integer
function SetPlayerStrength(playerid, value) end

--- Returns the strength of a player.
--- @param playerid integer
--- @return integer strength
function GetPlayerStrength(playerid) end

--- Sets the position coordinates of a player.
--- @param playerid integer
--- @param x number
--- @param y number
--- @param z number
function SetPlayerPos(playerid, x, y, z) end

--- Returns the position coordinates of a player.
--- @param playerid integer
--- @return number x, number y, number z
function GetPlayerPos(playerid) end

--- Sets the angle of a player.
--- @param playerid integer
--- @param angle integer
function SetPlayerAngle(playerid, angle) end

--- Returns the angle of a player.
--- @param playerid integer
--- @return integer angle
function GetPlayerAngle(playerid) end

--- Returns if a player is connected.
--- @param playerid integer
--- @return 0|1 connected
function IsPlayerConnected(playerid) end

--- Sets the name of a player which is shown in the playerlist and the focus.
--- @param playerid integer
--- @param name string
function SetPlayerName(playerid, name) end

--- Returns the player's name.
--- @param playerid integer
--- @return string name
function GetPlayerName(playerid) end

--- Sets the instance of a player, e.g. "PC_HERO".
--- @param playerid integer
--- @param instance string
function SetPlayerInstance(playerid, instance) end

--- Returns the instance of a player, e.g. "PC_HERO".
--- @param playerid integer
--- @return string instance
function GetPlayerInstance(playerid) end

--- @param playerid integer
--- @param skillid skilltype
--- @param value integer
function SetPlayerSkillWeapon(playerid, skillid, value) end

--- @param playerid integer
--- @param skillid skilltype
--- @return integer value
function GetPlayerSkillWeapon(playerid, skillid) end

--- @param playerid integer
--- @param mode weaponmodetype
function SetPlayerWeaponMode(playerid, mode) end

--- @param playerid integer
--- @return weaponmodetype weaponmode
function GetPlayerWeaponMode(playerid) end

--- @param playerid integer
--- @param bodyModel string
--- @param bodyTextureID integer
--- @param headModel string
--- @param headTextureID integer
function SetPlayerAdditionalVisual(playerid,
	bodyModel, bodyTextureID, headModel, headTextureID) end

--- @param playerid integer
--- @return string bodyModel, integer bodyTextureID
--- @return string headModel, integer headTextureID
function GetPlayerAdditionalVisual(playerid) end

--- Sets the fatness of a player.
--- @param playerid integer
--- @param fatness number
function SetPlayerFatness(playerid, fatness) end

--- Returns the fatness of a player.
--- @param playerid integer
--- @return number fatness
function GetPlayerFatness(playerid) end

--- Sets the value of the skill acrobatic.
--- @param playerid integer
--- @param mode 0|1
function SetPlayerAcrobatic(playerid, mode) end

--- Returns the value of the skill acrobatic.
--- @param playerid integer
--- @return 0|1 value
function GetPlayerAcrobatic(playerid) end

--- Sets the level of a player. This function calls `OnPlayerChangeLevel`.
--- @param playerid integer
--- @param value integer
function SetPlayerLevel(playerid, value) end

--- Returns the level of a player.
--- @param playerid integer
--- @return integer level
function GetPlayerLevel(playerid) end

--- Sets the experience of a player.
--- @param playerid integer
--- @param value integer
function SetPlayerExperience(playerid, value) end

--- Returns the experience of a player.
--- @param playerid integer
--- @return integer experience
function GetPlayerExperience(playerid) end

--- Sets the experience that a player needs to reach a new level.
--- @param playerid integer
--- @param value integer
function SetPlayerExperienceNextLevel(playerid, value) end

--- Returns the experience that a player needs to reach a new level. Note that
--- you must change the level manually via `SetPlayerLevel()`, this function
--- only changes the displayed value in the character manu.
--- @param playerid integer
--- @return integer value
function GetPlayerExperienceNextLevel(playerid) end

--- Sets the learn points of a player.
--- @param playerid integer
--- @param value integer
function SetPlayerLearnPoints(playerid, value) end

--- Returns the learn points of a player.
--- @param playerid integer
--- @return integer value
function GetPlayerLearnPoints(playerid) end

--- If set to 1, the player is freezed so that he can't move.
--- @param playerid integer
--- @param toggle 0|1
function FreezePlayer(playerid, toggle) end

--- Equips the armor with the given instance.
--- @param playerid integer
--- @param instance string
function EquipArmor(playerid, instance) end

--- @param playerid integer
function UnequipArmor(playerid) end

--- Equips the melee weapon (1H/2H) with the given instance.
--- @param playerid integer
--- @param instance string
function EquipMeleeWeapon(playerid, instance) end

--- Unequips the melee weapon (1H/2H).
--- @param playerid integer
function UnequipMeleeWeapon(playerid) end

--- Equips the ranged weapon (Bow, CBow) with the given instance.
--- @param playerid integer
--- @param instance string
function EquipRangedWeapon(playerid, instance) end

--- Unequips the ranged weapon (Bow, CBow) with the given instance.
--- @param playerid integer
function UnequipRangedWeapon(playerid) end

--- Equips the belt with the given instance.
--- @param playerid integer
--- @param instance string
function EquipBelt(playerid, instance) end

--- Unequips the belt with the given instance.
--- @param playerid integer
function UnequipBelt(playerid) end

--- Equips the amulet with the given instance.
--- @param playerid integer
--- @param instance string
function EquipAmulet(playerid, instance) end

--- Unequips the amulet with the given instance.
--- @param playerid integer
function UnequipAmulet(playerid) end

--- Equips the helmet with the given instance.
--- @param playerid integer
--- @param instance string
function EquipHelmet(playerid, instance) end

--- Unequips the helmet with the given instance.
--- @param playerid integer
function UnequipHelmet(playerid) end

--- Equips the ring with the given instance.
---
--- This function will throw an error if there is no free slot available.
--- In that case you have to first unequip on of the rings before calling
--- this function.
---
--- Note that you can't unequip a ring by calling this function with `"NULL"`,
--- you have to use `UnequipRing()` or `UnequipItem()` to unequip it.
--- @param playerid integer
--- @param instance string
function EquipRing(playerid, instance) end

--- Unequips the ring at the given slot which can be 0 or 1.
--- @param playerid integer
--- @param slot 0|1
function UnequipRing(playerid, slot) end

--- Equips the item with that instance, which can be of every item type.
---
--- Note: EquipItem() doesn't work for NPC's!
--- @param playerid integer
--- @param instance string
function EquipItem(playerid, instance) end

--- @param playerid integer
--- @param instance string
function UnequipItem(playerid, instance) end

--- Returns the instance of the player's equipped armor. If set to `"NULL"`, the
--- player doesn't wear an armor.
--- @param playerid integer
--- @return string armor
function GetEquippedArmor(playerid) end

--- Returns the instance of the player's equipped melee weapon. If set to
--- `"NULL"`, the player hasn't equipped one.
--- @param playerid integer
--- @return string weapon
function GetEquippedMeleeWeapon(playerid) end

--- Returns the instance of the player's equipped ranged weapon. If set to
--- `"NULL"`, the player hasn't equipped one.
--- @param playerid integer
--- @return string weapon
function GetEquippedRangedWeapon(playerid) end

--- Returns the instance of the player's equipped belt. If set to `"NULL"`, the
--- player doesn't wear a belt.
--- @param playerid integer
--- @return string belt
function GetEquippedBelt(playerid) end

--- Returns the instance of the player's equipped amulet. If set to `"NULL"`,
--- the player doesn't wear an amulet.
--- @param playerid integer
--- @return string amulet
function GetEquippedAmulet(playerid) end

--- Returns the instance of the player's equipped helmet. If set to `"NULL"`,
--- the player doesn't wear a helmet.
--- @param playerid integer
--- @return string helmet
function GetEquippedHelmet(playerid) end

--- Returns the equipped ring at the given slot which can be 0 or 1, or `"NULL"`
--- if there is no equipped ring.
--- @param playerid integer
--- @param slot 0|1
function GetEquippedRing(playerid, slot) end

--- @param playerid integer
---@param world string
---@param waypoint? string Default = "START"
function SetPlayerWorld(playerid, world, waypoint) end

--- Returns the name of the world in which the player is. If he is currently
--- changing his world, this function returns `"CHANGING WORLD"`. If he isn't
--- connected, `"NO WORLD"` is returned.
--- @param playerid integer
--- @return string world
function GetPlayerWorld(playerid) end

--- Sets the magic level of a player.
--- @param playerid integer
--- @param value 0|1|2|3|4|5|6
function SetPlayerMagicLevel(playerid, value) end

--- Returns the magic level of a player.
--- @param playerid integer
--- @return 0|1|2|3|4|5|6 level
function GetPlayerMagicLevel(playerid) end

--- Spawns a player. You must first spawn a player to change his attributes,
--- give him items etc. If he isn't spawned, he can't see nor interact with
--- other players.
--- @param playerid integer
--- @param x? number Default = 0
--- @param y? number Default = 0
--- @param z? number Default = 0
--- @param angle? integer Default = 0
function SpawnPlayer(playerid, x, y, z, angle) end

--- @param playerid integer
--- @param science sciencetype
--- @param value integer
function SetPlayerScience(playerid, science, value) end

--- @param playerid integer
--- @param science sciencetype
--- @return integer value
function GetPlayerScience(playerid, science) end

--- @param playerid integer
--- @param amount integer
function SetPlayerGold(playerid, amount) end

--- Returns the amount of gold (`ITMI_GOLD`) that a player has.
--- @param playerid integer
--- @return integer value
function GetPlayerGold(playerid) end

--- Returns if `playerid1` currently streams `playerid2`.
function IsStreamed(playerid1, playerid2) end

--- Returns the ID of the player in a player's focus. If no player is focused,
--- it returns -1.
--- @param playerid integer
--- @return integer focus
function GetFocus(playerid) end

--- Lets offenderid hit victimid in his current weapon mode without animation,
--- if victimid streams offenderid.
---
--- Note: victimid can't be an NPC.
--- @param offenderid integer
--- @param victimid integer
function HitPlayer(offenderid, victimid) end

--- Returns if a player is dead.
--- @param playerid integer
--- @return 0|1 dead
function IsDead(playerid) end

--- Returns if a player is unconscious.
--- @param playerid integer
--- @return 0|1 unconscious
function IsUnconscious(playerid) end

--- Returns if a player is spawned.
--- @param playerid integer
--- @return 0|1 spawned
function IsSpawned(playerid) end

--- Returns the player's IP or nil if he's not a valid player.
---
--- Note: Don't use the IP address to identify a player!
--- The IP address of a host changes regularly (usually every 24h),
--- and in the case of a CGNAT (f.e. when the player uses DS Lite),
--- there could be hundreds of players behind that IP address.
--- @param playerid integer
--- @return string? ip
function GetPlayerIP(playerid) end

--- Returns the MAC address of the given player or nil if he isn't connected or
--- is an NPC.
--- @param playerid integer
--- @return string? macaddress
function GetMacAddress(playerid) end

--- Returns the hash of the volume ID of the player's `C:` drive or nil if he's
--- not a valid player.
---
--- The volume ID can be used to identify a player just like the Mac address.
--- But be aware that it's very easy to change it. For a more secure version,
--- see `GetPlayerSerial()`.
--- @param playerid integer
--- @return string? volumeid
function GetPlayerVolumeID(playerid) end

--- Returns the serial number of the player's hard disk on which Windows is
--- installed, or nil if the serial number couldn't be retrieved or the player
--- is not valid. If the disk doesn't have a serial number, this string is
--- empty.
---
--- The serial number can be used to identify a player, just like the Mac
--- address, except that it can't be changed and is thus more secure for bans.
---
--- **Important: Please note that not all manufacturers assign a unique serial
--- number to their hard disks. F. e., all Intel Optane disks seem to have the
--- serial number "Optane_0000".**
--- @param playerid integer
--- @return string? serial
function GetPlayerSerial(playerid) end

--- Enables or disables the ability to drop items even if you are dead.
--- @param toggle 0|1
function EnableDropAfterDeath(toggle) end

--- @param playerid integer
--- @param scaleX number
--- @param scaleY number
--- @param scaleZ number
function SetPlayerScale(playerid, scaleX, scaleY, scaleZ) end

--- @param playerid integer
--- @return number scaleX, number scaleY, number scaleZ
function GetPlayerScale(playerid) end

--- Enables or disables the Marvin mode for the player.
--- @param playerid integer
--- @param toggle 0|1
function EnableMarvin(playerid, toggle) end

--- Returns if the marvin mode is activated.
--- @param playerid integer
--- @return 0|1 activated
function IsMarvinEnabled(playerid) end

--- Scans in the player's Gothic root directory path for all files with the
--- given extension (e.g. ".vdf") and calls `OnPlayerFileList accordingly`. To
--- scan for more than one extension, you can separate them by semicolons, e.g.
--- ".vdf;.mod". If you specify "" as file extension, it will return all files
--- in the directory.
--- @param playerid integer
--- @param path string
--- @param fileExtensions? string Default = ""
function GetFileList(playerid, path, fileExtensions) end

--- @param intensity number
function SetBleedThreshold(intensity) end

--- @param playerid integer
--- @param x number
--- @param z number
function TurnPlayerTo(playerid, x, z) end

--- @param playerid integer
--- @return integer guildid
function GetPlayerGuild(playerid) end

--- @param playerid integer
--- @param guildid integer
function SetPlayerGuild(playerid, guildid) end

--- Returns the screen resolution of a player.
--- @param playerid integer
--- @return integer width, integer height
function GetPlayerResolution(playerid) end

--- Returns the instance of the item in the player's left hand. If he doesn't
--- hold anything, returns `"NULL"`.
--- @param playerid integer
--- @return string instance
function GetLeftHand(playerid) end

--- Returns the instance of the item in the player's right hand. If he doesn't
--- hold anything, returns `"NULL"`.
--- @param playerid integer
--- @return string instance
function GetRightHand(playerid) end

--- Returns the instance of the item that the player currently holds in the
--- specified hand. If set to `"NULL"`, the player doesn't hold one.
--- @param playerid integer
--- @param hand handtype
function GetPlayerHand(playerid, hand) end

--- @param playerid integer
--- @param hand handtype
--- @param instance string
function SetPlayerHand(playerid, hand, instance) end
