--- @meta
--- GMP API callbacks documentation.
--- Wiki: @see https://gitlab.com/Reveares/GMP/-/wikis/Callbacks


function OnGamemodeInit() end

function OnFilterscriptInit() end

--- @param playerid integer
function OnPlayerConnect(playerid) end

--- @param playerid integer
function OnPlayerSpawn(playerid) end

--- Called when a player (`killerid`) kills another player or NPC (`victimid`).
--- The first callback is called when there is no killer, so via scripts by
--- setting the HP to 0 or through `HitPlayer()`, or through fall damage.
---
--- The second callback is a so-called "sync callback". You can use it to
--- check if e.g. the player could get killed with that amount of HP. If you
--- handle this callback in your gamemode and return `false`, the kill isn't
--- synchronized to other players. Every other value (including `nil` and no
--- return value) will synchronize the kill.
--- @param victimid integer
--- @param unused integer
--- @param killerid integer
--- @param unused2 integer
--- @param health integer?
--- @return false?
function OnPlayerDeath(victimid, unused, killerid, unused2, health) end

--- Called when a player (`killerid`) unconscious another player or NPC
--- (`victimid`). The first callback is called when victimid got unconscious
--- through `HitPlayer()`.
---
--- The second callback is a so-called "sync callback". You can use it to check
--- if e.g. the player could get unconscious with that amount of HP. If you
--- handle this callback in your gamemode and return `false`, it isn't
--- synchronized to other players. Every other value (including `nil` and no
--- return value) will synchronize it.
--- @param victimid integer
--- @param unused integer
--- @param killerid integer
--- @param unused2 integer
--- @param health integer?
--- @return false?
function OnPlayerUnconscious(victimid, unused, killerid, unused2, health) end

--- @param playerid integer
--- @param reason integer
function OnPlayerDisconnect(playerid, reason) end

--- @param playerid integer
--- @param text string
function OnPlayerText(playerid, text) end

--- @param playerid integer
--- @param cmdtext string
function OnPlayerCommandText(playerid, cmdtext) end

--- Called when a player enters some text into an input field. If he closes it
--- manually (e.g. by pressing Esc), `text` is set to `nil`.
--- @param playerid integer
--- @param text? string
function OnPlayerInputField(playerid, text) end

--- Called when a player (`offenderid`) hits another player or NPC (`victimid`).
--- The first callback is called when the hit was done via scripts
--- (`HitPlayer()`).
---
--- The second callback is a so-called "sync callback". You can use it to check
--- if e.g. the damage is valid. If you handle this callback in your gamemode
--- and return `false`, the hit isn't synchronized to other players and the
--- health doesn't change. Every other value (including `nil` and no return
--- value) will synchronize the hit.
--- @param victimid integer
--- @param offenderid integer
--- @param damage integer?
--- @return false?
function OnPlayerHit(victimid, offenderid, damage) end

--- @param playerid integer
function OnPlayerStandUp(playerid) end

--- Is called if a player is changing his world. `GetPlayerWorld()` will return
--- `"CHANGING WORLD"`, until he entered the new world in
--- `OnPlayerEnterWorld()`.
--- @param playerid integer
--- @param newWorld string
function OnPlayerChangeWorld(playerid, newWorld) end

--- Is called if a player enters a world.
--- @param playerid integer
--- @param world string
function OnPlayerEnterWorld(playerid, world) end

--- **[Note]: The rotation is totally pointless, because a player can't set the
--- rotation of an item if he drops it. These values are always 0 and will be
--- removed in the next version.**
--- @param playerid integer
---@param itemid integer
---@param item_instance string
---@param amount integer
---@param posX number
---@param posY number
---@param posZ number
---@param world string
---@param rotX number
---@param rotY number
---@param rotZ number
function OnPlayerDropItem(playerid, itemid, item_instance,
	amount, posX, posY, posZ, world, rotX, rotY, rotZ) end

--- @param playerid integer
--- @param itemid integer
--- @param item_instance string
--- @param amount integer
--- @param x number
--- @param y number
--- @param z number
--- @param world string
function OnPlayerTakeItem(playerid, itemid,
	item_instance, amount, x, y, z, world) end

--- @param playerid integer
function OnPlayerUpdate(playerid) end

--- @param playerid integer
--- @param slot integer
--- @param item_instance string
--- @param amount integer
--- @param equipped integer
function OnPlayerResponseItem(playerid,
	slot, item_instance, amount, equipped) end

--- @param playerid integer
--- @param item_instance string
--- @param amount integer
--- @param equipped 0|1
--- @param checkid integer
function OnPlayerHasItem(playerid, item_instance, amount, equipped, checkid) end

--- This callback is called if a player presses or releases a key on his
--- keyboard. If it is pressed, `keyDown` is set to the according keycode,
---  otherwise it's `keyUp`.
---
--- **Important**
---
--- By default, this callback is disabled. To activate it, you must first call
--- `Enable_OnPlayerKey()`, or `SetPlayerEnable_OnPlayerKey()` to activate it
--- for a specific player.
--- @param playerid integer
--- @param keyDown integer
--- @param keyUp integer
function OnPlayerKey(playerid, keyDown, keyUp) end

--- Called when a player clicks a mouse button and the cursor is visible.
--- Valid buttons are `MB_LEFT` for the left button and `MB_RIGHT` for the
--- right button. If the button is pressed, pressed is set to 1, it it is
--- released, pressed is set to 0. posX and posY specify the position of the
--- cursor when the button was pressed, in virtual pixels in the range 0-8192.
--- @param playerid integer
--- @param button integer
--- @param pressed integer
--- @param posX integer
--- @param posY integer
function OnPlayerMouse(playerid, button, pressed, posX, posY) end

--- @param playerid integer
--- @param weaponMode weaponmodetype
function OnPlayerWeaponMode(playerid, weaponMode) end

--- @param playerid integer
--- @param focusid integer
function OnPlayerFocus(playerid, focusid) end

--- @param playerid integer
--- @param itemInstance string
--- @param amount integer
--- @param hand handtype
function OnPlayerUseItem(playerid, itemInstance, amount, hand) end

--- Called when a player interacts with a mob. If he triggers it, `trigger` is
--- set to 1, otherwise 0. `scheme` indicates the type of the triggered mob,
--- which normally (for client-side mobs) is the first sequence of the mob's
--- visual name until the first underscore. For example, if the visual name is
--- "DOOR_OCR__135", the scheme is "DOOR". `objectName` can be used to identify
--- the mob further, but it's mostly an empty string. The object name of a mob
--- can be changed in the Spacer, or, for server-side mobs, via `Mob.Create()`
--- or `Mob.SetObjectName()`.
--- @param playerid integer
--- @param scheme string
--- @param objectName string
--- @param trigger 0|1
function OnPlayerTriggerMob(playerid, scheme, objectName, trigger) end

--- @param playerid integer
--- @param pathFile string
--- @param hash string
function OnPlayerMD5File(playerid, pathFile, hash) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeMaxHealth(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeHealth(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeMaxMana(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeMana(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeStrength(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeDexterity(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeSkillWeapon(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param science integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeScience(playerid, science, newValue, oldValue) end

--- @param playerid integer
--- @param newBodyModel string
--- @param newBodyTexture integer
--- @param newHeadModel string
--- @param newHeadTexture integer
--- @param oldBodyModel string
--- @param oldBodyTexture integer
--- @param oldHeadModel string
--- @param oldHeadTexture integer
function OnPlayerChangeAdditionalVisual(playerid,
	newBodyModel, newBodyTexture, newHeadModel, newHeadTexture,
	oldBodyModel, oldBodyTexture, oldHeadModel, oldHeadTexture) end

--- @param playerid integer
--- @param newValue number
--- @param oldValue number
function OnPlayerChangeFatness(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeAcrobatic(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeGold(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newWalk string
--- @param oldWalk string
function OnPlayerChangeWalk(playerid, newWalk, oldWalk) end

--- @param playerid integer
--- @param newArmor string
--- @param oldArmor string
function OnPlayerChangeArmor(playerid, newArmor, oldArmor) end

--- @param playerid integer
--- @param newWeapon string
--- @param oldWeapon string
function OnPlayerChangeMeleeWeapon(playerid, newWeapon, oldWeapon) end

--- @param playerid integer
--- @param newWeapon string
--- @param oldWeapon string
function OnPlayerChangeRangedWeapon(playerid, newWeapon, oldWeapon) end

--- @param playerid integer
--- @param newBelt string
--- @param oldBelt string
function OnPlayerChangeBelt(playerid, newBelt, oldBelt) end

--- @param playerid integer
--- @param newAmulet string
--- @param oldAmulet string
function OnPlayerChangeAmulet(playerid, newAmulet, oldAmulet) end

--- @param playerid integer
--- @param newHelmet string
--- @param oldHelmet string
function OnPlayerChangeHelmet(playerid, newHelmet, oldHelmet) end

--- @param playerid integer
--- @param isActive integer
--- @param itemInstance string
function OnPlayerEquipRing(playerid, isActive, itemInstance) end

--- @param playerid integer
--- @param isActive integer
--- @param itemInstance string
function OnPlayerEquipRune(playerid, isActive, itemInstance) end

--- @param playerid integer
--- @param newInstance string
--- @param oldInstance string
function OnPlayerChangeInstance(playerid, newInstance, oldInstance) end

--- @param playerid integer
function OnPlayerOpenInventory(playerid) end

--- @param playerid integer
function OnPlayerCloseInventory(playerid) end

--- @param playerid integer
--- @param itemInstance string
function OnPlayerSpellSetup(playerid, itemInstance) end

--- @param playerid integer
--- @param itemInstance string
function OnPlayerSpellCast(playerid, itemInstance) end

--- @param playerid integer
--- @param newLevel integer
--- @param oldLevel integer
function OnPlayerChangeMagicLevel(playerid, newLevel, oldLevel) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeLevel(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeExperience(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeExperienceNextLevel(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newValue integer
--- @param oldValue integer
function OnPlayerChangeLearnPoints(playerid, newValue, oldValue) end

--- @param playerid integer
--- @param newX number
--- @param newY number
--- @param newZ number
--- @param oldX number
--- @param oldY number
--- @param oldZ number
function OnPlayerChangeScale(playerid, newX, newY, newZ, oldX, oldY, oldZ) end

--- @param playerid integer
--- @param path string
--- @param fileExtensions string
--- @param fileCounter integer
--- @param fileList string
function OnPlayerFileList(playerid, path, fileExtensions, fileCounter, fileList) end

--- @param oldHour hourtype
--- @param oldMinute minutetype
--- @param newHour hourtype
--- @param newMinute minutetype
function OnServerWorldTime(oldHour, oldMinute, newHour, newMinute) end

--- @param playerid integer
--- @param toggle 0|1
function OnPlayerTogglePause(playerid, toggle) end

--- @param playerid integer
--- @param toggle 0|1
function OnPlayerToggleChat(playerid, toggle) end

--- @param playerid integer
--- @param toggle 0|1
function OnPlayerToggleDebugMenu(playerid, toggle) end

--- @param playerid integer
--- @param toggle 0|1
function OnPlayerTogglePlayerList(playerid, toggle) end

function OnServerTick() end
