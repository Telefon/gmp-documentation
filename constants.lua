--- @meta
--- GMP API constants documentation.
--- Wiki: @see https://gitlab.com/Reveares/GMP/-/wikis/Constants

--------------------------------------------------------------------------------
------------------------------------- Item -------------------------------------
--------------------------------------------------------------------------------

ITEM_UNSYNCHRONIZED = -1
ITEM_GIFT = -2
ITEM_CONTAINER = -3

--------------------------------------------------------------------------------
---------------------------------- Weaponmode ----------------------------------
--------------------------------------------------------------------------------

WEAPON_NONE = 0
WEAPON_FIST = 1
WEAPON_ERROR_NOT_USED = 2
WEAPON_1H = 3
WEAPON_2H = 4
WEAPON_BOW = 5
WEAPON_CBOW = 6
WEAPON_MAGIC = 7

--- @alias weaponmodetype
---| `WEAPON_NONE`
---| `WEAPON_FIST`
---| `WEAPON_ERROR_NOT_USED`
---| `WEAPON_1H`
---| `WEAPON_2H`
---| `WEAPON_BOW`
---| `WEAPON_CBOW`
---| `WEAPON_MAGIC`


--------------------------------------------------------------------------------
------------------------------------ Skill -------------------------------------
--------------------------------------------------------------------------------

SKILL_1H = 0
SKILL_2H = 1
SKILL_BOW = 2
SKILL_CBOW = 3

--- @alias skilltype
---| `SKILL_1H`
---| `SKILL_2H`
---| `SKILL_BOW`
---| `SKILL_CBOW`


--------------------------------------------------------------------------------
------------------------------------- Hand -------------------------------------
--------------------------------------------------------------------------------

LEFT_HAND = 0
RIGHT_HAND = 1

--- @alias handtype
---| `LEFT_HAND`
---| `RIGHT_HAND`


--------------------------------------------------------------------------------
----------------------------------- Weather ------------------------------------
--------------------------------------------------------------------------------

WEATHER_RAIN = 0
WEATHER_SNOW = 1

--- @alias weathertype
---| `WEATHER_RAIN`
---| `WEATHER_SNOW`


--------------------------------------------------------------------------------
------------------------------------- Mob --------------------------------------
--------------------------------------------------------------------------------

OCMOB = 0
OCMOBINTER = 1
OCMOBLADDER = 2
OCMOBDOOR = 3
OCMOBCONTAINER = 4
OCMOBBED = 5

--- @alias ocmobtype
---| `OCMOB`
---| `OCMOBINTER`
---| `OCMOBLADDER`
---| `OCMOBDOOR`
---| `OCMOBCONTAINER`
---| `OCMOBBED`


--------------------------------------------------------------------------------
----------------------------------- Science ------------------------------------
--------------------------------------------------------------------------------

SCIENCE_OPENING_LOCKS = 0
SCIENCE_SNEAKING = 1
SCIENCE_REGENERATE = 2
SCIENCE_FIREMASTER = 3
SCIENCE_THIEF = 4
SCIENCE_SLOSH_BLADES = 5
SCIENCE_CREATING_RUNES = 6
SCIENCE_ALCHEMY = 7
SCIENCE_COLLECTING_TROPHIES = 8
SCIENCE_FOREIGNLANGUAGE = 9
SCIENCE_WISP = 10
SCIENCE_CUSTOM_TALENT_1 = 11
SCIENCE_CUSTOM_TALENT_2 = 12
SCIENCE_CUSTOM_TALENT_3 = 13

--- @alias sciencetype
---| `SCIENCE_OPENING_LOCKS`
---| `SCIENCE_SNEAKING`
---| `SCIENCE_REGENERATE`
---| `SCIENCE_FIREMASTER`
---| `SCIENCE_THIEF`
---| `SCIENCE_SLOSH_BLADES`
---| `SCIENCE_CREATING_RUNES`
---| `SCIENCE_ALCHEMY`
---| `SCIENCE_COLLECTING_TROPHIES`
---| `SCIENCE_FOREIGNLANGUAGE`
---| `SCIENCE_WISP`
---| `SCIENCE_CUSTOM_TALENT_1`
---| `SCIENCE_CUSTOM_TALENT_2`
---| `SCIENCE_CUSTOM_TALENT_3`

--------------------------------------------------------------------------------
--------------------------------- Mouse Button ---------------------------------
--------------------------------------------------------------------------------

MB_LEFT = 0
MB_RIGHT = 1

--- @alias mousebuttontype
---| `MB_LEFT`
---| `MB_RIGHT`


--------------------------------------------------------------------------------
------------------------------------- Keys -------------------------------------
--------------------------------------------------------------------------------

KEY_ESCAPE = 0
KEY_1 = 1
KEY_2 = 2
KEY_3 = 3
KEY_4 = 4
KEY_5 = 5
KEY_6 = 6
KEY_7 = 7
KEY_8 = 8
KEY_9 = 9
KEY_0 = 10
KEY_MINUS = 11
KEY_EQUALS = 12
KEY_BACK = 13
KEY_TAB = 14
KEY_Q = 15
KEY_W = 16
KEY_E = 17
KEY_R = 18
KEY_T = 19
KEY_Y = 20
KEY_U = 21
KEY_I = 22
KEY_O = 23
KEY_P = 24
KEY_LBRACKET = 25
KEY_RBRACKET = 26
KEY_RETURN = 27
KEY_LCONTROL = 28
KEY_A = 29
KEY_S = 30
KEY_D = 31
KEY_F = 32
KEY_G = 33
KEY_H = 34
KEY_J = 35
KEY_K = 36
KEY_L = 37
KEY_SEMICOLON = 38
KEY_APOSTROPHE = 39
KEY_TILDE = 40
KEY_LSHIFT = 41
KEY_BACKSLASH = 42
KEY_Z = 43
KEY_X = 44
KEY_C = 45
KEY_V = 46
KEY_B = 47
KEY_N = 48
KEY_M = 49
KEY_COMMA = 50
KEY_PERIOD = 51
KEY_SLASH = 52
KEY_RSHIFT = 53
KEY_MULTIPLY = 54
KEY_LMENU = 55
KEY_SPACE = 56
KEY_CAPITAL = 57
KEY_F1 = 58
KEY_F2 = 59
KEY_F3 = 60
KEY_F4 = 61
KEY_F5 = 62
KEY_F6 = 63
KEY_F7 = 64
KEY_F8 = 65
KEY_F9 = 66
KEY_F10 = 67
KEY_NUMLOCK = 68
KEY_SCROLL = 69
KEY_NUMPAD7 = 70
KEY_NUMPAD8 = 71
KEY_NUMPAD9 = 72
KEY_SUBTRACT = 73
KEY_NUMPAD4 = 74
KEY_NUMPAD5 = 75
KEY_NUMPAD6 = 76
KEY_ADD = 77
KEY_NUMPAD1 = 78
KEY_NUMPAD2 = 79
KEY_NUMPAD3 = 80
KEY_NUMPAD0 = 81
KEY_DECIMAL = 82
KEY_F11 = 83
KEY_F12 = 84
KEY_NUMPADENTER = 85
KEY_RCONTROL = 86
KEY_DIVIDE = 87
KEY_RMENU = 88
KEY_PAUSE = 89
KEY_HOME = 90
KEY_UP = 91
KEY_PRIOR = 92
KEY_LEFT = 93
KEY_RIGHT = 94
KEY_END = 95
KEY_DOWN = 96
KEY_NEXT = 97
KEY_INSERT = 98
KEY_DELETE = 99
KEY_POWER = 100
KEY_SLEEP = 101
KEY_WAKE = 102

--- @alias keytype
---| `KEY_ESCAPE`
---| `KEY_1`
---| `KEY_2`
---| `KEY_3`
---| `KEY_4`
---| `KEY_5`
---| `KEY_6`
---| `KEY_7`
---| `KEY_8`
---| `KEY_9`
---| `KEY_0`
---| `KEY_MINUS`
---| `KEY_EQUALS`
---| `KEY_BACK`
---| `KEY_TAB`
---| `KEY_Q`
---| `KEY_W`
---| `KEY_E`
---| `KEY_R`
---| `KEY_T`
---| `KEY_Y`
---| `KEY_U`
---| `KEY_I`
---| `KEY_O`
---| `KEY_P`
---| `KEY_LBRACKET`
---| `KEY_RBRACKET`
---| `KEY_RETURN`
---| `KEY_LCONTROL`
---| `KEY_A`
---| `KEY_S`
---| `KEY_D`
---| `KEY_F`
---| `KEY_G`
---| `KEY_H`
---| `KEY_J`
---| `KEY_K`
---| `KEY_L`
---| `KEY_SEMICOLON`
---| `KEY_APOSTROPHE`
---| `KEY_TILDE`
---| `KEY_LSHIFT`
---| `KEY_BACKSLASH`
---| `KEY_Z`
---| `KEY_X`
---| `KEY_C`
---| `KEY_V`
---| `KEY_B`
---| `KEY_N`
---| `KEY_M`
---| `KEY_COMMA`
---| `KEY_PERIOD`
---| `KEY_SLASH`
---| `KEY_RSHIFT`
---| `KEY_MULTIPLY`
---| `KEY_LMENU`
---| `KEY_SPACE`
---| `KEY_CAPITAL`
---| `KEY_F1`
---| `KEY_F2`
---| `KEY_F3`
---| `KEY_F4`
---| `KEY_F5`
---| `KEY_F6`
---| `KEY_F7`
---| `KEY_F8`
---| `KEY_F9`
---| `KEY_F10`
---| `KEY_NUMLOCK`
---| `KEY_SCROLL`
---| `KEY_NUMPAD7`
---| `KEY_NUMPAD8`
---| `KEY_NUMPAD9`
---| `KEY_SUBTRACT`
---| `KEY_NUMPAD4`
---| `KEY_NUMPAD5`
---| `KEY_NUMPAD6`
---| `KEY_ADD`
---| `KEY_NUMPAD1`
---| `KEY_NUMPAD2`
---| `KEY_NUMPAD3`
---| `KEY_NUMPAD0`
---| `KEY_DECIMAL`
---| `KEY_F11`
---| `KEY_F12`
---| `KEY_NUMPADENTER`
---| `KEY_RCONTROL`
---| `KEY_DIVIDE`
---| `KEY_RMENU`
---| `KEY_PAUSE`
---| `KEY_HOME`
---| `KEY_UP`
---| `KEY_PRIOR`
---| `KEY_LEFT`
---| `KEY_RIGHT`
---| `KEY_END`
---| `KEY_DOWN`
---| `KEY_NEXT`
---| `KEY_INSERT`
---| `KEY_DELETE`
---| `KEY_POWER`
---| `KEY_SLEEP`
---| `KEY_WAKE`

--------------------------------------------------------------------------------
----------------------------------- General ------------------------------------
--------------------------------------------------------------------------------

--- @alias hourtype 0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23

-- luacheck: push ignore
--- @alias minutetype 0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|59
-- luacheck: pop
